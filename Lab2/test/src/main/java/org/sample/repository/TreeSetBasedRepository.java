package org.sample.repository;

import java.util.Set;
import java.util.TreeSet;

public class TreeSetBasedRepository<T extends Comparable<T>> implements InMemoryRepository<T> {
    private Set<T> set;

    public TreeSetBasedRepository() {
        this.set = new TreeSet<>();
    }

    @Override
    public void add(T element) {
        this.set.add(element);
    }

    @Override
    public boolean contains(T element) {
        return this.set.contains(element);
    }

    @Override
    public void remove(T element) {
        this.set.remove(element);
    }

    public Set<T> getSet() {
        return set;
    }

    public void setSet(Set<T> set) {
        this.set = set;
    }
}
