package org.sample.repository;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class ConcurrentHashMapBasedRepository<T> implements InMemoryRepository<T> {
    private ConcurrentMap<Integer, T> hashMap;

    public ConcurrentHashMapBasedRepository() {
        this.hashMap = new ConcurrentHashMap<>();
    }

    @Override
    public void add(T element) {
        this.hashMap.put(element.hashCode(), element);
    }

    @Override
    public boolean contains(T element) {
        return this.hashMap.containsKey(element.hashCode());
    }

    @Override
    public void remove(T element) {
        this.hashMap.remove(element.hashCode());
    }

    public ConcurrentMap<Integer, T> getHashMap() {
        return hashMap;
    }

    public void setHashMap(ConcurrentMap<Integer, T> hashMap) {
        this.hashMap = hashMap;
    }
}
