package org.sample.repository;

import org.eclipse.collections.impl.set.sorted.mutable.TreeSortedSet;

public class EclipseTreeSortedSetBasedRepository<T> implements InMemoryRepository<T> {
    private TreeSortedSet<T> set;

    public EclipseTreeSortedSetBasedRepository() {
        this.set = new TreeSortedSet<>();
    }

    @Override
    public void add(T element) {
        this.set.add(element);
    }

    @Override
    public boolean contains(T element) {
        return this.set.contains(element);
    }

    @Override
    public void remove(T element) {
        this.set.remove(element);
    }

    public TreeSortedSet<T> getSet() {
        return set;
    }

    public void setSet(TreeSortedSet<T> set) {
        this.set = set;
    }
}
