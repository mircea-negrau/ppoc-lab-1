package org.sample.repository;

import it.unimi.dsi.fastutil.objects.ObjectRBTreeSet;

public class FastUtilSetBasedRepository<T> implements InMemoryRepository<T> {
    private ObjectRBTreeSet<T> set;

    public FastUtilSetBasedRepository() {
        this.set = new ObjectRBTreeSet<>();
    }

    @Override
    public void add(T element) {
        this.set.add(element);
    }

    @Override
    public boolean contains(T element) {
        return this.set.contains(element);
    }

    @Override
    public void remove(T element) {
        this.set.remove(element);
    }

    public ObjectRBTreeSet<T> getSet() {
        return set;
    }

    public void setSet(ObjectRBTreeSet<T> set) {
        this.set = set;
    }
}
