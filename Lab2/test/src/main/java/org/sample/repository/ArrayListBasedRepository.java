package org.sample.repository;

import java.util.ArrayList;
import java.util.List;

public class ArrayListBasedRepository<T> implements InMemoryRepository<T> {
    private List<T> list;

    public ArrayListBasedRepository() {
        this.list = new ArrayList<T>();
    }

    @Override
    public void add(T element) {
        this.list.add(element);
    }

    @Override
    public boolean contains(T element) {
        return this.list.contains(element);
    }

    @Override
    public void remove(T element) {
        this.list.remove(element);
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }
}
