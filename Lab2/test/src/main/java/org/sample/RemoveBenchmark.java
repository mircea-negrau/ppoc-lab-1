package org.sample;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.profile.GCProfiler;
import org.openjdk.jmh.profile.StackProfiler;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.openjdk.jmh.runner.options.TimeValue;
import org.sample.models.Order;
import org.sample.repository.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

public class RemoveBenchmark extends BaseBenchmark {
    @Setup(Level.Invocation)
    public void setUp() {
        orders = new ArrayList<>();
        arrayList = new ArrayListBasedRepository<>();
        concurrentHashMap = new ConcurrentHashMapBasedRepository<>();
        eclipseTreeSortedSet = new EclipseTreeSortedSetBasedRepository<>();
        fastUtilSet = new FastUtilSetBasedRepository<>();
        hashSet = new HashSetBasedRepository<>();
        treeSet = new TreeSetBasedRepository<>();

        for (int index = 0; index < size; index++) {
            orders.add(new Order(index, index, index));
        }

        Collections.shuffle(orders);
        for (Order order : orders) {
            arrayList.add(order);
            concurrentHashMap.add(order);
            eclipseTreeSortedSet.add(order);
            fastUtilSet.add(order);
            hashSet.add(order);
            treeSet.add(order);
        }
    }

    @Benchmark
    public void remove_ArrayList() {
        for (Order order : orders) {
            arrayList.remove(order);
        }
    }

    @Benchmark
    public void remove_ConcurrentHashMap() {
        for (Order order : orders) {
            concurrentHashMap.remove(order);
        }
    }

    @Benchmark
    public void remove_EclipseTreeSortedSet() {
        for (Order order : orders) {
            eclipseTreeSortedSet.remove(order);
        }
    }

    @Benchmark
    public void remove_FastUtilSet() {
        for (Order order : orders) {
            fastUtilSet.remove(order);
        }
    }

    @Benchmark
    public void remove_HashSet() {
        for (Order order : orders) {
            hashSet.remove(order);
        }
    }

    @Benchmark
    public void remove_TreeSet() {
        for (Order order : orders) {
            treeSet.remove(order);
        }
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .resultFormat(ResultFormatType.CSV)
                .result("remove-results.csv")
                .addProfiler(StackProfiler.class)
                .addProfiler(GCProfiler.class)
                .include(RemoveBenchmark.class.getSimpleName())
                .forks(1)
                .warmupIterations(20)
                .warmupTime(TimeValue.seconds(1))
                .measurementIterations(20)
                .measurementTime(TimeValue.seconds(1))
                .timeUnit(TimeUnit.NANOSECONDS)
                .mode(Mode.AverageTime)
                .build();
        new Runner(opt).run();
    }
}
