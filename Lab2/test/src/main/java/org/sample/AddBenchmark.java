package org.sample;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.profile.GCProfiler;
import org.openjdk.jmh.profile.StackProfiler;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.openjdk.jmh.runner.options.TimeValue;
import org.sample.models.Order;
import org.sample.repository.*;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class AddBenchmark extends BaseBenchmark {
    @Setup(Level.Invocation)
    public void setUp() {
        orders = new ArrayList<>();
        arrayList = new ArrayListBasedRepository<>();
        concurrentHashMap = new ConcurrentHashMapBasedRepository<>();
        eclipseTreeSortedSet = new EclipseTreeSortedSetBasedRepository<>();
        fastUtilSet = new FastUtilSetBasedRepository<>();
        hashSet = new HashSetBasedRepository<>();
        treeSet = new TreeSetBasedRepository<>();

        for (int index = 0; index < size; index++) {
            orders.add(new Order(index, index, index));
        }
    }

    @Benchmark
    public void add_ArrayList() {
        for (Order order : orders) {
            arrayList.add(order);
        }
    }

    @Benchmark
    public void add_ConcurrentHashMap() {
        for (Order order : orders) {
            concurrentHashMap.add(order);
        }
    }

    @Benchmark
    public void add_EclipseTreeSortedSet() {
        for (Order order : orders) {
            eclipseTreeSortedSet.add(order);
        }
    }

    @Benchmark
    public void add_FastUtilSet() {
        for (Order order : orders) {
            fastUtilSet.add(order);
        }
    }

    @Benchmark
    public void add_HashSet() {
        for (Order order : orders) {
            hashSet.add(order);
        }
    }

    @Benchmark
    public void add_TreeSet() {
        for (Order order : orders) {
            treeSet.add(order);
        }
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .resultFormat(ResultFormatType.CSV)
                .result("add-results.csv")
                .include(AddBenchmark.class.getSimpleName())
                .forks(1)
                .warmupIterations(20)
                .warmupTime(TimeValue.seconds(1))
                .measurementIterations(20)
                .measurementTime(TimeValue.seconds(1))
                .timeUnit(TimeUnit.NANOSECONDS)
                .mode(Mode.AverageTime)
                .build();
        new Runner(opt).run();
    }
}
