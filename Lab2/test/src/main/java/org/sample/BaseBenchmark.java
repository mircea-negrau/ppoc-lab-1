package org.sample;

import org.openjdk.jmh.annotations.*;
import org.sample.models.Order;
import org.sample.repository.*;

import java.util.ArrayList;

@State(Scope.Benchmark)
public abstract class BaseBenchmark {
    @Param({"1", "100", "1000"})
    public int size;

    public ArrayList<Order> orders;
    public ArrayListBasedRepository<Order> arrayList;
    public ConcurrentHashMapBasedRepository<Order> concurrentHashMap;
    public EclipseTreeSortedSetBasedRepository<Order> eclipseTreeSortedSet;
    public FastUtilSetBasedRepository<Order> fastUtilSet;
    public HashSetBasedRepository<Order> hashSet;
    public TreeSetBasedRepository<Order> treeSet;
}
