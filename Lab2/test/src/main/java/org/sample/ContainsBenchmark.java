package org.sample;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.profile.GCProfiler;
import org.openjdk.jmh.profile.StackProfiler;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.openjdk.jmh.runner.options.TimeValue;
import org.sample.models.Order;
import org.sample.repository.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

public class ContainsBenchmark extends BaseBenchmark {
    @Setup(Level.Invocation)
    public void setUp() {
        orders = new ArrayList<>();
        arrayList = new ArrayListBasedRepository<>();
        concurrentHashMap = new ConcurrentHashMapBasedRepository<>();
        eclipseTreeSortedSet = new EclipseTreeSortedSetBasedRepository<>();
        fastUtilSet = new FastUtilSetBasedRepository<>();
        hashSet = new HashSetBasedRepository<>();
        treeSet = new TreeSetBasedRepository<>();

        for (int index = 0; index < size; index++) {
            orders.add(new Order(index, index, index));
        }

        Collections.shuffle(orders);
        for (Order order : orders) {
            arrayList.add(order);
            concurrentHashMap.add(order);
            eclipseTreeSortedSet.add(order);
            fastUtilSet.add(order);
            hashSet.add(order);
            treeSet.add(order);
        }
    }

    @Benchmark
    public void contains_ArrayList() {
        for (Order order : orders) {
            arrayList.contains(order);
        }
    }

    @Benchmark
    public void contains_ConcurrentHashMap() {
        for (Order order : orders) {
            concurrentHashMap.contains(order);
        }
    }

    @Benchmark
    public void contains_EclipseTreeSortedSet() {
        for (Order order : orders) {
            eclipseTreeSortedSet.contains(order);
        }
    }

    @Benchmark
    public void contains_FastUtilSet() {
        for (Order order : orders) {
            fastUtilSet.contains(order);
        }
    }

    @Benchmark
    public void contains_HashSet() {
        for (Order order : orders) {
            hashSet.contains(order);
        }
    }

    @Benchmark
    public void contains_TreeSet() {
        for (Order order : orders) {
            treeSet.contains(order);
        }
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .resultFormat(ResultFormatType.CSV)
                .result("contains-results.csv")
                .addProfiler(GCProfiler.class)
                .include(ContainsBenchmark.class.getSimpleName())
                .forks(1)
                .warmupIterations(20)
                .warmupTime(TimeValue.seconds(1))
                .measurementIterations(20)
                .measurementTime(TimeValue.seconds(1))
                .timeUnit(TimeUnit.NANOSECONDS)
                .mode(Mode.AverageTime)
                .build();
        new Runner(opt).run();
    }
}
