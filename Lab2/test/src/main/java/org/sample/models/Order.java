package org.sample.models;

import java.util.Objects;

public class Order implements Comparable<Order> {
    private int id;
    private int price;
    private int quantity;

    public Order(int id, int price, int quantity) {
        this.id = id;
        this.price = price;
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object other) {
        if (!other.getClass().equals(Order.class)) return false;
        Order otherOrder = (Order) other;
        return otherOrder.id == id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(Order.class, id, price, quantity);
    }

    @Override
    public String toString() {
        return "Order(id=" + id + ",price=" + price + ",quantity=" + quantity;
    }

    @Override
    public int compareTo(Order other) {
        return Integer.compare(this.getId(), other.getId());
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
