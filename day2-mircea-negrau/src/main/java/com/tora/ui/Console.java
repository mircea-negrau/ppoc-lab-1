package com.tora.ui;

import com.tora.domain.commands.*;
import com.tora.domain.commands.base.ICommand;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Console {
    private final Scanner in = new Scanner(System.in);

    private final Map<String, ICommand> commands = new HashMap<>() {{
        put("1", new AddCommand());
        put("2", new SubtractCommand());
        put("3", new DivideCommand());
        put("4", new MultiplyCommand());
        put("5", new MinCommand());
        put("6", new MaxCommand());
        put("7", new SqrtCommand());
        put("0", new ExitCommand());
    }};

    public Console() {
        run();
    }

    private void printMenu() {
        for (var command : commands.entrySet()) {
            System.out.println(command.getKey() + ". " + command.getValue().getName());
        }
        System.out.println("Command: ");
    }

    public void run() {
        while (true) {
            printMenu();
            String command = in.nextLine().strip();
            try {
                var targetCommand = commands.get(command);
                targetCommand.read();
                targetCommand.execute();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
