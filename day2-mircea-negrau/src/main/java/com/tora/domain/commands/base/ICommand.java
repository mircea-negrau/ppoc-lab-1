package com.tora.domain.commands.base;

import com.tora.domain.arguments.IArguments;

public interface ICommand extends IArguments {
    double execute();

    String getName();
}
