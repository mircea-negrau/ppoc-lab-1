package com.tora.domain.commands;

import com.tora.domain.commands.base.EmptyCommand;

public class ExitCommand extends EmptyCommand {
    @Override
    public double execute() {
        System.exit(0);
        return 0;
    }

    @Override
    public String getName() {
        return "Exit";
    }
}
