package com.tora.domain.commands;

import com.tora.domain.commands.base.TupleCommand;

public class AddCommand extends TupleCommand {
    @Override
    public double execute() {
        return firstArgument + secondArgument;
    }

    @Override
    public String getName() {
        return "Add";
    }
}
