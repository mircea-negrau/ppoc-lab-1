package com.tora.domain.commands.base;

import com.tora.domain.arguments.NoArgument;

public abstract class EmptyCommand extends NoArgument implements ICommand {
    public abstract double execute();
}
