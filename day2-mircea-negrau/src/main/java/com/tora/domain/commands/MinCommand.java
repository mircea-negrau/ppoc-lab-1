package com.tora.domain.commands;

import com.tora.domain.commands.base.TupleCommand;

public class MinCommand extends TupleCommand {
    @Override
    public double execute() {
        return Math.min(firstArgument, secondArgument);
    }

    @Override
    public String getName() {
        return "Min";
    }
}
