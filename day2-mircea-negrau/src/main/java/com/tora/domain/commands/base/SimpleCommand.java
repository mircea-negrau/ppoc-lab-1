package com.tora.domain.commands.base;

import com.tora.domain.arguments.SingleArgument;

public abstract class SimpleCommand extends SingleArgument implements ICommand {
    public abstract double execute();
}
