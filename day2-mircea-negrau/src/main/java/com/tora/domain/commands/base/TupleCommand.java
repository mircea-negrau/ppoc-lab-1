package com.tora.domain.commands.base;

import com.tora.domain.arguments.DoubleArguments;

public abstract class TupleCommand extends DoubleArguments implements ICommand {
    public abstract double execute();
}
