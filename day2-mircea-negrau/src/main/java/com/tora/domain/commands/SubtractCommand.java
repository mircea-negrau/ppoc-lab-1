package com.tora.domain.commands;

import com.tora.domain.commands.base.TupleCommand;

public class SubtractCommand extends TupleCommand {
    @Override
    public double execute() {
        return firstArgument - secondArgument;
    }

    @Override
    public String getName() {
        return "Subtract";
    }
}
