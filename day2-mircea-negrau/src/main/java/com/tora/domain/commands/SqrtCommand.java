package com.tora.domain.commands;

import com.tora.domain.commands.base.SimpleCommand;

public class SqrtCommand extends SimpleCommand {
    @Override
    public double execute() {
        return Math.sqrt(argument);
    }

    @Override
    public String getName() {
        return "Sqrt";
    }
}
