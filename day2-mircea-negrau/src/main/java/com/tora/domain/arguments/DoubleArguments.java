package com.tora.domain.arguments;

import java.util.Scanner;

public class DoubleArguments implements IArguments {
    private final Scanner in = new Scanner(System.in);

    protected double firstArgument;
    protected double secondArgument;

    public void setFirstArgument(int firstArgument) {
        this.firstArgument = firstArgument;
    }

    public void setSecondArgument(int secondArgument) {
        this.secondArgument = secondArgument;
    }

    public void read() {
        System.out.println("First number: ");
        firstArgument = in.nextDouble();
        System.out.println("Second number: ");
        secondArgument = in.nextDouble();
    }
}
