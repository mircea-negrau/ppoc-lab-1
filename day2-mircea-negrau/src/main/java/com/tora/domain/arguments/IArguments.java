package com.tora.domain.arguments;

public interface IArguments {
    void read();
}
