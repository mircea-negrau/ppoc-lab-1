package com.tora.domain.arguments;

import java.util.Scanner;

public class SingleArgument implements IArguments {
    private final Scanner in = new Scanner(System.in);
    protected double argument;

    public void setArgument(int argument) {
        this.argument = argument;
    }

    public void read() {
        System.out.println("Number: ");
        argument = in.nextDouble();
    }
}
