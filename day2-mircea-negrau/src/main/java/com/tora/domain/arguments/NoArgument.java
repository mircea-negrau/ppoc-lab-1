package com.tora.domain.arguments;

public class NoArgument implements IArguments {
    public void read(){
        // Should be empty
    }
}
