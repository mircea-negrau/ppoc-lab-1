package com.tora;

import com.tora.domain.commands.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CommandExecutionTests {
    @Test
    public void addCommandExecution() {
        AddCommand addCommand = new AddCommand();
        addCommand.setFirstArgument(5);
        addCommand.setSecondArgument(3);
        assertEquals(8, addCommand.execute(), 0.0);
    }

    @Test
    public void divideCommandExecution() {
        DivideCommand divideCommand = new DivideCommand();
        divideCommand.setFirstArgument(9);
        divideCommand.setSecondArgument(3);
        assertEquals(3, divideCommand.execute(), 0.0);
    }

    @Test
    public void maxCommandExecution() {
        MaxCommand maxCommand = new MaxCommand();
        maxCommand.setFirstArgument(9);
        maxCommand.setSecondArgument(3);
        assertEquals(9, maxCommand.execute(), 0.0);
    }

    @Test
    public void minCommandExecution() {
        MinCommand minCommand = new MinCommand();
        minCommand.setFirstArgument(9);
        minCommand.setSecondArgument(3);
        assertEquals(3, minCommand.execute(), 0.0);
    }

    @Test
    public void multiplyCommandExecution() {
        MultiplyCommand multiplyCommand = new MultiplyCommand();
        multiplyCommand.setFirstArgument(9);
        multiplyCommand.setSecondArgument(3);
        assertEquals(27, multiplyCommand.execute(), 0.0);
    }

    @Test
    public void sqrtCommandExecution() {
        SqrtCommand sqrtCommand = new SqrtCommand();
        sqrtCommand.setArgument(9);
        assertEquals(3, sqrtCommand.execute(), 0.0);
    }

    @Test
    public void subtractCommandExecution() {
        SubtractCommand subtractCommand = new SubtractCommand();
        subtractCommand.setFirstArgument(9);
        subtractCommand.setSecondArgument(3);
        assertEquals(6, subtractCommand.execute(), 0.0);
    }
}
