package com.tora;

import static org.junit.Assert.assertEquals;

import com.tora.domain.commands.*;
import org.junit.Test;

public class CommandNamesTests {
    @Test
    public void addCommandName() {
        AddCommand addCommand = new AddCommand();
        assertEquals("Add", addCommand.getName());
    }

    @Test
    public void divideCommandName() {
        DivideCommand divideCommand = new DivideCommand();
        assertEquals("Divide", divideCommand.getName());
    }

    @Test
    public void maxCommandName() {
        MaxCommand maxCommand = new MaxCommand();
        assertEquals("Max", maxCommand.getName());
    }

    @Test
    public void minCommandName() {
        MinCommand minCommand = new MinCommand();
        assertEquals("Min", minCommand.getName());
    }

    @Test
    public void multiplyCommandName() {
        MultiplyCommand multiplyCommand = new MultiplyCommand();
        assertEquals("Multiply", multiplyCommand.getName());
    }

    @Test
    public void sqrtCommandName() {
        SqrtCommand sqrtCommand = new SqrtCommand();
        assertEquals("Sqrt", sqrtCommand.getName());
    }

    @Test
    public void subtractCommandName() {
        SubtractCommand subtractCommand = new SubtractCommand();
        assertEquals("Subtract", subtractCommand.getName());
    }
}
